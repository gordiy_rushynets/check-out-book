//
//  main.cpp
//  checkout
//
//  Created by Gordiy Rushynets on 2/19/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <string>
#include <memory>


using namespace std;

class Student
{
protected:
    string surname;
    string faculty;
    string group;
public:
    string get_surname(){return surname;}
    string get_faculty(){return faculty;}
    string get_group(){return group;}
    
    friend istream &operator>>(istream &in, Student &student)
    {
        return in >> student.surname >> student.faculty >> student.group;
    }
    
    friend ostream &operator<<(ostream &out, const Student &student)
    {
        return out << ' ' << student.surname << ' ' << student.faculty << ' ' << student.group;
    }
};


class Discipline
{
private:
    int id;
    string discipline_name;
    string lector;
    double grade;
public:
    Discipline():id(0),discipline_name(""),lector(""),grade(0.0){}
    
    friend std::istream &operator>>(std::istream &in, Discipline &discipline)
    {
        return in >> discipline.id >> discipline.discipline_name >> discipline.lector >> discipline.grade;
    }
    
    friend std::ostream &operator<<(std::ostream &out, const Discipline &discipline)
    {
        return out << discipline.id << ' ' << discipline.discipline_name << ' ' << discipline.lector << ' ' << discipline.grade;
    }
    
    int get_id()
    {
        return id;
    }
    
    string get_discipline_name()
    {
        return discipline_name;
    }
    
    string get_lector()
    {
        return lector;
    }
    
    double get_grade()
    {
        return grade;
    }
    
    void set_id(int ID)
    {
        id = ID;
    }
    
    void set_discipline_name(string new_discipline_name)
    {
        discipline_name = new_discipline_name;
    }
    
    void set_lector(string new_lector)
    {
        lector = new_lector;
    }
    
    void set_rating(double rating)
    {
        grade = rating;
    }
};

class CheckOutBook
{
private:
    string lastName;
    string faculty;
    string group;
    
    Discipline *discplines;
public:
    CheckOutBook() :
    lastName(""),
    faculty(""),
    group("")
    {
        discplines = new Discipline[1];
        for(int i = 0; i < 1; i++)
        {
            discplines[i].set_id(0);
            discplines[i].set_discipline_name("");
            discplines[i].set_lector("");
            discplines[i].set_rating(0.0);
        }
    }
    
    friend std::istream &operator>>(std::istream &in, CheckOutBook &checkoutBook)
    {
        return in >> checkoutBook.lastName >> checkoutBook.faculty >> checkoutBook.group;
    }
    
    friend std::ostream &operator<<(std::ostream &out, const CheckOutBook &checkoutBook)
    {
        return out << checkoutBook.lastName << ' ' << checkoutBook.faculty << ' ' << checkoutBook.group;
    }
    
    Discipline &operator[](int index)
    {
        return discplines[index];
    }
    
    void set_last_name(string surname)
    {
        lastName = surname;
    }
    
    void set_faculty(string new_faculty)
    {
        faculty = new_faculty;
    }
    
    void set_group(string new_group)
    {
        group = new_group;
    }
    
    ~CheckOutBook(){
        delete [] discplines;
    }
};


int main()
{
    int count_students;
    // Student *students = read_student();
    ifstream fin_student("/Users/gordiy/cpp/checkOutBook/checkOutBook/student.txt");
    ifstream fin_discipline("/Users/gordiy/cpp/checkOutBook/checkOutBook/discipline.txt");
    
    fin_student >> count_students;
    
    CheckOutBook *check_out_book = new CheckOutBook[count_students];
    
    for(int i = 0; i < count_students; i++)
    {
        string last_name;
        fin_student >> last_name;
        
        check_out_book[i].set_last_name(last_name);
        
        string faculty;
        fin_student >> faculty;
        
        check_out_book[i].set_faculty(faculty);
        
        string group;
        fin_student >> group;
        
        check_out_book[i].set_group(group);
        
        int count_disciplines;
        int count_disciplines_for_student;
        fin_discipline >> count_disciplines;
        
        count_disciplines_for_student = 2 + rand() & count_disciplines;
        
        for (int j = 0; j < count_disciplines_for_student; j++) {
            int id;
            fin_discipline >> id;
            
            cout << id << endl;
            check_out_book[i][j].set_id(id);
            
            string name;
            fin_discipline >> name;
            
            cout << name << endl;
            check_out_book[i][j].set_discipline_name(name);
            
            string lector;
            fin_discipline >> lector;
            
            cout << lector << endl;
            check_out_book[i][j].set_lector(lector);
            
            double rating;
            fin_discipline >> rating;
            
            cout << rating << endl;
            check_out_book[i][j].set_rating(rating);
        }
    }
    
    
    system("pause");
    return 0;
}
